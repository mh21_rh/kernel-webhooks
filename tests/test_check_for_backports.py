"""Tests for check_for_backports."""
from unittest import mock

from tests import fakes_jira
from tests.helpers import KwfTestCase
from webhook.defs import GitlabURL
from webhook.defs import READY_FOR_MERGE_LABEL
from webhook.rh_metadata import Projects
from webhook.rhissue import RHIssue
from webhook.utils import check_for_backports


@mock.patch.dict('os.environ', {'RH_METADATA_EXTRA_PATHS': 'tests/assets/rh_projects_sandbox.yaml',
                                'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'})
class TestCheckForBackports(KwfTestCase):
    """Tests for the various helper functions."""

    def test_find_issues_for_fix_version(self):
        mock_jira = mock.Mock()
        issue = self.make_jira_issue('RHEL-101')
        mock_jira.search_issues.return_value = [issue]
        results = check_for_backports.find_issues_for_fix_version(mock_jira, 'rhel-8.7.0')
        expected_jql = check_for_backports.BASE_JQL + 'AND fixVersion = rhel-8.7.0'
        mock_jira.search_issues.assert_called_with(jql_str=expected_jql, maxResults=500,
                                                   fields=check_for_backports.JIRA_FIELDS)
        self.assertEqual(results, [issue])

    def test_build_assignment_comment(self):
        mock_author = mock.Mock()
        mock_author.name = "Shadow Man"
        mock_author.username = "shadowman"
        mock_approver = mock.Mock()
        mock_approver.name = "Some Reviewer"
        mock_approver.username = "somerev"
        mock_y_mr_url = "https://gitlab.com/foo/bar/-/merge_reqeusts/1234"
        expected = ('Please review this automated z-stream Merge Request, as you were either an '
                    'author or approver of the y-stream Merge Request from which it was '
                    'generated.\n\n'
                    'Y-Stream Merge Request: https://gitlab.com/foo/bar/-/merge_reqeusts/1234  \n'
                    'Y-Stream MR Author: Shadow Man  \n'
                    'Y-Stream MR Approvers: Some Reviewer  \n'
                    '\n'
                    '/assign @rhelmaintainer\n'
                    '/assign_reviewer @shadowman @somerev')
        val = check_for_backports.build_assignment_comment('rhelmaintainer', [mock_approver],
                                                           mock_author, mock_y_mr_url)
        self.assertEqual(val, expected)

    def test_get_zstream_maintainer(self):
        mock_session = mock.Mock()
        mock_sst = mock.Mock()
        mock_sst.maintainers = ['shadowman']
        mock_session.owners.get_matching_subsystem_by_name.return_value = mock_sst

        mock_mr = mock.Mock()
        mock_mr.gl_mr = mock.Mock()
        val = check_for_backports.get_zstream_maintainer(mock_mr, mock_session)
        self.assertEqual(val, 'shadowman')

        # No maintainer found
        mock_session.update_webhook_comment.reset_mock()
        mock_session.owners.get_matching_subsystem_by_name.return_value = None
        val = check_for_backports.get_zstream_maintainer(mock_mr, mock_session)
        self.assertIsNone(val)

    @mock.patch('webhook.utils.check_for_backports.build_assignment_comment',
                mock.Mock(return_value="this is a string"))
    @mock.patch('webhook.utils.check_for_backports.get_zstream_maintainer', mock.Mock())
    @mock.patch('webhook.utils.check_for_backports.BaseMR.new')
    def test_file_gitlab_tps_reports(self, mock_new_mr):
        mock_zmr = mock.Mock()
        mock_zmr.gl_mr = mock.Mock()
        mock_new_mr.return_value = mock_zmr
        mock_session = mock.Mock()
        mock_ymr = mock.Mock()
        mock_approver = mock.Mock()
        mock_approver.name = "Some Reviewer"
        mock_approver.username = "somerev"
        mock_ymr.approved_by = [mock_approver]
        check_for_backports.file_gitlab_tps_reports(mock_session, mock.Mock(), mock_ymr)
        mock_session.update_webhook_comment.assert_called_with(mock_zmr.gl_mr, "this is a string")

    @mock.patch('webhook.rhissue.RHIssue.mr_urls', new_callable=mock.PropertyMock)
    @mock.patch('webhook.utils.check_for_backports.file_gitlab_tps_reports', mock.Mock())
    @mock.patch('webhook.rhissue.RHIssue.cloned_from', new_callable=mock.PropertyMock)
    @mock.patch('webhook.utils.check_for_backports.Backport')
    def test_attempt_backport(self, mock_libbackport, cloned_from, mr_urls):
        mock_session = mock.Mock()
        mock_session.jira = mock.Mock()
        mock_session.rh_projects = Projects()
        mock_session.args.testing = False

        mock_ymr = mock.Mock()
        mock_ymr.commits = {'abcd1234': 'whatever'}
        mock_ymr.url = "https://gitlab.com/foo/bar/-/merge_reqeusts/1234"
        mock_ymr.branch.name = "main"
        mock_backport = mock.Mock()
        mock_libbackport.return_value = mock_backport

        issue = self.make_jira_issue('RHEL-101')
        mock_rhi = RHIssue.new_from_ji(ji=issue, jira=mock_session.jira, mrs=None,
                                       projects=mock_session.rh_projects)
        mock_yrhi = mock.Mock()
        mock_yrhi.id = "99"
        cloned_from.return_value = [mock_yrhi]

        # A backport already exists, bail out
        mr_urls.return_value = ['there is a backport']
        ret = check_for_backports.attempt_backport(mock_rhi, mock_session, mock_ymr)
        self.assertFalse(ret)

        # Okay, no backport linked in MR yet
        mr_urls.return_value = []

        # We failed at backporting
        mock_backport.try_to_backport.return_value = False
        check_for_backports.attempt_backport(mock_rhi, mock_session, mock_ymr)
        mock_backport.add_info_to_jira_issue.assert_called_with(success=False)
        mock_backport.cleanup.assert_called()

        # We were successful at backporting
        mock_backport.try_to_backport.return_value = True
        check_for_backports.attempt_backport(mock_rhi, mock_session, mock_ymr)
        mock_backport.add_info_to_jira_issue.assert_called_with(success=True)
        mock_backport.rhissue.ji.update.assert_called_once()

    @mock.patch('webhook.utils.check_for_backports.attempt_backport')
    @mock.patch('webhook.utils.check_for_backports.YstreamMR.new')
    @mock.patch('webhook.rhissue.RHIssue.mr_urls', new_callable=mock.PropertyMock)
    @mock.patch('webhook.rhissue.RHIssue.cloned_from', new_callable=mock.PropertyMock)
    @mock.patch('webhook.utils.check_for_backports.make_rhissues')
    def test_find_backport_work(self, mock_rhissues, cloned_from, mr_urls, new_mr, mock_attempt):
        mock_session = mock.Mock()
        mock_session.jira = mock.Mock()
        mock_session.rh_projects = Projects()

        issue = self.make_jira_issue('RHEL-101')
        rhi = RHIssue.new_from_ji(ji=issue, jira=mock_session.jira, mrs=None,
                                  projects=mock_session.rh_projects)
        mock_rhissues.return_value = [rhi]
        clone_issue = self.make_jira_issue('RHEL-102')
        clone_rhi = RHIssue.new_from_ji(ji=clone_issue, jira=mock_session.jira, mrs=None,
                                        projects=mock_session.rh_projects)
        clone_issue2 = self.make_jira_issue('RHEL-103')
        clone_rhi2 = RHIssue.new_from_ji(ji=clone_issue2, jira=mock_session.jira, mrs=None,
                                         projects=mock_session.rh_projects)

        mr_url1 = GitlabURL('https://gitlab.com/group/project/-/merge_requests/19')
        mr_url2 = GitlabURL('https://gitlab.com/group/project/-/merge_requests/21')

        # Too many y-stream issues found
        cloned_from.return_value = [clone_rhi, clone_rhi2]
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='WARNING') as logs:
            check_for_backports.find_backport_work(mock_session, issue)
            self.assertIn("I have no idea what to do with an issue (RHEL-101) that is apparently "
                          "cloned from 2 different issues: ['RHEL-102', 'RHEL-103']",
                          logs.output[-1])

        # No y-stream issue found
        cloned_from.return_value = []
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='INFO') as logs:
            check_for_backports.find_backport_work(mock_session, issue)
            self.assertIn("Issue RHEL-101 does not have a y-stream issue we can find",
                          logs.output[-1])

        # Okay, we have just the one expected issue we're cloned from
        cloned_from.return_value = [clone_rhi]

        # ...but we have too many MRs linked to the y-stream issue
        mr_urls.return_value = [mr_url1, mr_url2]
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='WARNING') as logs:
            check_for_backports.find_backport_work(mock_session, issue)
            self.assertIn("Cowardly refusing to try to backport from a y-stream issue (RHEL-101) "
                          "with multiple Merge Requests linked to it: ['group/project/!19', "
                          "'group/project/!21']", logs.output[-1])

        # Okay, now no y-stream MR linked to issue yet
        mr_urls.return_value = []
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='INFO') as logs:
            check_for_backports.find_backport_work(mock_session, issue)
            self.assertIn("Issue RHEL-101's lead stream issue has no MR attached.", logs.output[-1])

        # Okay, we actually found something we can do a backport attempt with
        mr_urls.return_value = [mr_url1]
        mock_ymr = mock.Mock()
        mock_ymr.commits = True
        mock_ymr.labels = [READY_FOR_MERGE_LABEL]
        new_mr.return_value = mock_ymr
        check_for_backports.find_backport_work(mock_session, issue)
        mock_attempt.assert_called_with(rhi, mock_session, mock_ymr)

    @mock.patch.dict('os.environ', {'RHKERNEL_SRC': '/path/to/kernel/source'})
    @mock.patch('webhook.utils.check_for_backports.find_backport_work', mock.Mock())
    @mock.patch('webhook.session.SessionRunner.new')
    def test_main(self, mock_new_session):
        mock_session = mock.Mock()
        mock_session.rh_projects = Projects()
        mock_new_session.return_value = mock_session
        mock_session.args.projects = []

        mock_args = '-j RHEL-666'.split()
        mock_session.args.jira = 'RHEL-666'
        mock_session.args.fix_versions = []
        mock_session.jira.search_issues.return_value = [fakes_jira.JI666]
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='INFO') as logs:
            check_for_backports.main(mock_args)
            self.assertIn(f"Evaluating cli issues: {[fakes_jira.JI666]}", logs.output[-2])
            self.assertIn("Successful backports: 1", logs.output[-1])

        mock_args = '-f rhel-8.6.0.z'.split()
        mock_session.args.jira = ''
        mock_session.args.fix_versions = ['rhel-8.6.0.z']
        mock_session.jira.search_issues.return_value = [fakes_jira.JI666]
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='INFO') as logs:
            check_for_backports.main(mock_args)
            self.assertIn("Got issue list of 1 items for rhel-8.6.0.z", logs.output[-3])
            self.assertIn(f"Evaluating rhel-8.6.0.z issues: {[fakes_jira.JI666]}", logs.output[-2])
            self.assertIn("Successful backports: 1", logs.output[-1])

        mock_args = ''
        mock_session.args.jira = ''
        mock_session.args.fix_versions = []
        mock_session.args.projects = ['redhat/rhel/src/kernel/rhel-8-sandbox', 'foo/bar/blah']
        mock_session.jira.search_issues.return_value = [fakes_jira.JI666]
        with self.assertLogs('cki.webhook.utils.check_for_backports', level='INFO') as logs:
            check_for_backports.main(mock_args)
            self.assertIn("No project found for project namespace: foo/bar/blah", logs.output[-4])
            self.assertIn("Got issue list of 1 items for rhel-8.6.0.z", logs.output[-3])
            self.assertIn(f"Evaluating rhel-8.6.0.z issues: {[fakes_jira.JI666]}", logs.output[-2])
            self.assertIn("Successful backports: 1", logs.output[-1])
