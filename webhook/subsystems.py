"""Add kernel subsystem topics as labels to an MR."""
from dataclasses import dataclass
import sys
import typing

from cki_lib import logger

from . import common
from .base_mr import BaseMR
from .base_mr_mixins import CommitsMixin
from .base_mr_mixins import DependsMixin
from .base_mr_mixins import DiffsMixin
from .base_mr_mixins import OwnersMixin
from .defs import CODE_CHANGED_PREFIX
from .defs import Label
from .defs import MrScope
from .session import SessionRunner
from .session_events import GitlabMREvent
from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.subsystems')

EXT_CI_LABEL_PREFIX = 'ExternalCI'  # without colons
SUBSYS_LABEL_PREFIX = 'Subsystem:'  # with its single colon


@dataclass(repr=False)
class SubsysMR(DiffsMixin, OwnersMixin, CommitsMixin, DependsMixin, BaseMR):
    # pylint: disable=too-many-ancestors
    """An MR object with owners."""

    @property
    def current_ss_names(self):
        """Return the set of Subsystem: names currently on the MR."""
        return {lbl.removeprefix(SUBSYS_LABEL_PREFIX) for lbl in self.labels if
                lbl.startswith(SUBSYS_LABEL_PREFIX) and lbl.count(':') == 1}

    @property
    def expected_ss_names(self):
        """Return the expected set of Subsystem: names for the MR."""
        return {s.subsystem_label for s in self.owners_subsystems if s.subsystem_label}

    @property
    def stale_ss_names(self):
        """Return the set of current_subsystem_names that are not in the expected set."""
        return self.current_ss_names - self.expected_ss_names

    @property
    def expected_ss_labels(self):
        """Return the list of expected Subsystem: labels for this MR."""
        return [Label(f'{SUBSYS_LABEL_PREFIX}{name}') for name in self.expected_ss_names]

    @property
    def current_ci_labels(self):
        """Return the set of ExternalCI labels currently on the MR."""
        # This excludes the overall single-scoped ExternalCI label.
        return {label for label in self.labels_with_prefix(EXT_CI_LABEL_PREFIX) if label.scoped > 1}

    @property
    def current_ci_names(self):
        """Return the set of ExternalCI names currently on the MR."""
        return {label.primary for label in self.current_ci_labels}

    @property
    def expected_ci_names(self):
        """Return the expected set of ExternalCI names for the MR."""
        if self.draft:
            return set()
        return {name for s in self.owners_subsystems for name in s.ready_for_merge_label_deps}

    @property
    def missing_ci_labels(self):
        """Return the set of expected ExternalCI labels which are not currently found on the MR."""
        return {MrScope.NEEDS_TESTING.label(f'{EXT_CI_LABEL_PREFIX}::{name}') for
                name in self.expected_ci_names - self.current_ci_names}

    @property
    def current_ci_scope(self):
        """Return the MrScope of the existing ExternalCI label, or None."""
        return next((label.scope for label in self.labels_with_prefix(EXT_CI_LABEL_PREFIX) if
                     label.scoped == 1), None)

    @property
    def expected_ci_scope(self):
        """Return the MrScope for the ExternalCI label."""
        scope = min((label.scope for label in self.expected_ci_labels), default=MrScope.OK)
        # If any of the expected ci labels are missing then we can't be better than NeedsTesting.
        if self.missing_ci_labels and scope > MrScope.NEEDS_TESTING:
            return MrScope.NEEDS_TESTING
        # Don't ever set a Waived overall scope.
        return scope if scope is not MrScope.WAIVED else MrScope.OK

    @property
    def expected_ci_labels(self):
        """Return the set of expected ExternalCI:: labels for this MR. For a Draft this is set()."""
        if self.draft:
            return []
        # If the code changed then 'reset' all ci labels to NEEDS_TESTING.
        # Only consider the second value in the returned tuple!
        new_rev_label = f'{CODE_CHANGED_PREFIX}v{len(self.history)}'
        new_rev_label = new_rev_label if new_rev_label not in self.gl_mr.labels else None
        diff = self.code_changes()
        if new_rev_label and diff:
            return {MrScope.NEEDS_TESTING.label(f'{EXT_CI_LABEL_PREFIX}::{name}') for
                    name in self.expected_ci_names | self.current_ci_names}
        return self.current_ci_labels | self.missing_ci_labels


def update_mr(subsys_mr):
    """Process the SubsysMR."""
    LOGGER.info('Processing %s', subsys_mr.url)
    LOGGER.debug('All files affected: %s', subsys_mr.all_files)
    LOGGER.debug('Matching owners subsystems: %s', subsys_mr.owners_subsystems)
    LOGGER.info('Matching subsystems: %s', subsys_mr.expected_ss_names)

    mr_is_draft = subsys_mr.draft

    if stale_subsystems := subsys_mr.stale_ss_names:
        stale_ss_labels = [f'{SUBSYS_LABEL_PREFIX}{name}' for name in stale_subsystems]
        LOGGER.info('Removing stale subsystems labels: %s', stale_ss_labels)
        subsys_mr.remove_labels(stale_ss_labels)

    ss_labels = subsys_mr.expected_ss_labels
    ci_labels = list(subsys_mr.expected_ci_labels)  # This will always be empty for Draft MRs.
    LOGGER.info('Calculated subsystem labels: %s', ss_labels)
    if mr_is_draft:
        LOGGER.info('MR is in Draft state, not setting ExternalCI labels.')
    else:
        LOGGER.info('Calculated ExternalCI labels: %s', ci_labels)
        # Include the overall ExternalCI status label.
        ci_labels.append(subsys_mr.expected_ci_scope.label(EXT_CI_LABEL_PREFIX))
        LOGGER.info('Calculated overall ExternalCI label: %s', ci_labels[-1])
    if new_labels := set(ss_labels + ci_labels) - set(subsys_mr.labels):
        LOGGER.info('Labels to update: %s', new_labels)
        subsys_mr.add_labels(new_labels, remove_scoped=False)


def process_gl_event(
    _: dict,
    session: SessionRunner,
    event: GitlabMREvent | GitlabNoteEvent,
    **__: typing.Any
) -> None:
    """Handle a GL event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    # Get the MR data and set all the Subsystem: and ExternalCI labels.
    subsys_mr = SubsysMR.new(session, event.mr_url, source_path=session.args.repo_path)
    # If there doesn't seem to be any MR data then what are we doing here?
    if not subsys_mr.global_id:
        LOGGER.warning('No data for %s, ignoring.', subsys_mr)
        return
    update_mr(subsys_mr)


HANDLERS = {
    GitlabMREvent: process_gl_event,
    GitlabNoteEvent: process_gl_event,
}


def main(args):
    """Run main loop."""
    common.init_sentry()
    parser = common.get_arg_parser('SUBSYSTEMS')
    args = parser.parse_args(args)
    if not args.repo_path:
        raise RuntimeError('--repo-path (REPO_PATH) must be set')

    session = SessionRunner.new('subsystems', args=args, handlers=HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
