"""Library to consistently parse an MR or commit description."""
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from datetime import datetime
import os
import re
import typing

from cki_lib.logger import get_logger
from unidecode import unidecode

from webhook.defs import DCOState
from webhook.defs import JPFX
from webhook.defs import JiraKey
from webhook.graphql import GitlabGraph
from webhook.users import User

LOGGER = get_logger('cki.webhook.description')


@dataclass
class Description:
    """Parse commit description text tags into sets of IDs."""

    BUGZILLA_TAG = 'Bugzilla'
    JIRA_ISSUE_TAG = 'JIRA'
    INTERNAL_REGEX = re.compile(r'^JIRA: INTERNAL\s*$', re.MULTILINE)
    CVE_REGEX = re.compile(r'^CVE: (CVE-\d{4}-\d{4,7})\s*$', re.MULTILINE)
    DCO_REGEX = re.compile(r'^Signed-off-by: (.+) <(.+)>\s*$', re.MULTILINE)

    text: str = field(default='', repr=False)

    def __post_init__(self) -> None:
        """Deal with None for text string intput."""
        self.jira_server = os.environ.get('JIRA_SERVER', 'https://issues.redhat.com')

        if self.text is None:
            self.text = ''

        self.bugzilla: set[int] = self._parse_tag(self.BUGZILLA_TAG, self.text)
        self.jira_tags: set[JiraKey] = \
            self._parse_jira_tag(self.JIRA_ISSUE_TAG, self.text, self.jira_server)
        self.cve: set[str] = set(self.CVE_REGEX.findall(self.text))
        self.signoff: set[str] = set(self.DCO_REGEX.findall(self.text))
        self.marked_internal: bool = bool(self.INTERNAL_REGEX.findall(self.text))

    def __bool__(self) -> bool:
        """Return True if self.text is not empty, otherwise False."""
        return bool(self.text)

    def __eq__(self, other) -> bool:
        """Return True if the text is the same."""
        return self.text == other.text

    @staticmethod
    def _parse_tag(tag_prefix, text) -> set[int]:
        """Return the set of tag IDs as ints."""
        # tag_prefix: http://bugzilla.redhat.com/1234567
        # tag_prefix: https://bugzilla.redhat.com/show_bug.cgi?id=1234567
        pattern = r'^' + tag_prefix + \
            r': https?://bugzilla\.redhat\.com/(?:show_bug\.cgi\?id=)?(\d{4,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {int(tag) for tag in tag_regex.findall(text)}

    @staticmethod
    def _parse_jira_tag(tag_prefix, text, server) -> set[JiraKey]:
        """Return the set of tags as strs."""
        # tag_prefix: https://issues.redhat.com/browse/RHEL-1
        # tag_prefix: https://issues.redhat.com/projects/RHEL/issues/RHEL-1
        pattern = r'^' + tag_prefix + \
            rf': {server}/(?:browse|projects/RHEL/issues)/(' + \
            JPFX + r'\d{1,8})\s*$'
        tag_regex = re.compile(pattern, re.MULTILINE)
        return {JiraKey(tag) for tag in tag_regex.findall(text)}


@dataclass
class MRDescription(Description):
    """Parse MR description text tags into sets of IDs."""

    CC_REGEX = re.compile(r'^Cc:(?:| (.*)) <(.*)>\s*$', re.MULTILINE)
    DEPENDS_TAG = 'Depends'

    namespace: str = ''
    graphql: GitlabGraph | None = None

    def __post_init__(self) -> None:
        """Resolve the dependencies, if given a graphql object."""
        super().__post_init__()

        # We use a more specific regex for MRs if we have the namespace.
        mr_regex = self.mr_regex(self.namespace)

        self.cc: set[tuple[str, str]] = set(self.CC_REGEX.findall(self.text))
        self.depends_jiras: set[JiraKey] = set()
        self.depends_mrs: set[int] = set(int(mr_id) for mr_id in mr_regex.findall(self.text))
        self.depends_mrs_descriptions: dict[int, MRDescription] = {}

        # If we have a graphql instance and a namespace then we can try to troll through
        # all Depends MRs and get their JIRA tags.
        if self.graphql and self.namespace:
            self.depends_mrs_descriptions.update(
                self._depends_mrs_descriptions(self.namespace, self.graphql, self.depends_mrs)
            )

            self.depends_mrs.update(self.depends_mrs_descriptions.keys())

            self.depends_jiras.update(
                jira_id for mr_description in self.depends_mrs_descriptions.values() for
                jira_id in mr_description.jira_tags | mr_description.depends_jiras
            )

        # This is historic.
        self.depends: set[JiraKey] = self.depends_jiras

    def __eq__(self, other) -> bool:
        """Return True if the text, namespace, and _depends attribute values are the same."""
        return self.text == other.text and self.namespace == other.namespace and \
            self.depends_jiras == other.depends_jiras

    @classmethod
    def mr_regex(cls, namespace: str) -> re.Pattern:
        """Return the Gitlab MR Depends: regex Pattern for the given namespace."""
        if namespace:
            mr_regex_pattern = (
                r'^' + cls.DEPENDS_TAG +
                r': (?:https?://gitlab\.com/' + namespace + r'/-/merge_requests/|!)(\d+)\s*$'
            )
        else:
            mr_regex_pattern = r'^' + cls.DEPENDS_TAG + r': !(\d+)\s*$'

        return re.compile(mr_regex_pattern, re.MULTILINE)

    @staticmethod
    def _depends_mrs_descriptions(
        namespace: str,
        graphql: GitlabGraph,
        mr_ids: set[int]
    ) -> dict[int, 'MRDescription']:
        """Return a dict of all the depends_mrs descriptions."""
        mr_descriptions = {}
        todo_mrs = mr_ids
        max_level = 5

        while max_level and todo_mrs:
            max_level -= 1
            raw_desc_dict = graphql.get_mr_descriptions(namespace, todo_mrs)
            new_descriptions = {iid: MRDescription(raw_desc, namespace, graphql) for
                                iid, raw_desc in raw_desc_dict.items()}
            todo_mrs = {iid for desc in new_descriptions.values() for iid in desc.depends_mrs if
                        iid not in mr_descriptions}
            mr_descriptions.update(new_descriptions)

        return mr_descriptions


Signoff = typing.Tuple[str, str]


@dataclass(kw_only=True)
class Commit:
    # pylint: disable=too-many-instance-attributes
    """Properties of a commit."""

    author: User | None = field(default=None)
    author_email: str = ''
    author_name: str = ''
    committer_email: str = ''
    committer_name: str = ''
    date: datetime | None = field(default=None)
    description: Description | MRDescription = field(default_factory=Description)
    sha: str = ''
    title: str = ''
    input_dict: InitVar[dict] = {}
    zstream: bool = False
    diff: list = field(default_factory=list)

    def __post_init__(self, input_dict):
        """Set it up from the input_dict."""
        if input_dict:
            if author_dict := input_dict.get('author'):
                if self.author:
                    raise ValueError('input_dict has author key but author attrib is already set.')
                self.author = User(user_dict=author_dict)
            self.author_email = input_dict.get('authorEmail', '')
            self.author_name = input_dict.get('authorName', '')
            self.committer_email = input_dict.get('committerEmail', '')
            self.committer_name = input_dict.get('committerName', '')
            self.date = datetime.fromisoformat(input_dict['authoredDate'][:19]) if \
                'authoredDate' in input_dict else None
            self.description = Description(text=input_dict.get('description', ''))
            self.sha = input_dict.get('sha', '')
            self.title = input_dict.get('title', '')
            # Only present if CommitsDiffsMixin is overriding our _commits query
            self.diff = input_dict.get('diffs', [])
        # If GL has associated this commit with this User then we can assume the commit
        # authorEmail belongs to this User.
        if self.author and self.author_email:
            self.author.add_email(self.author_email)
        LOGGER.debug('Created %s', self)

    def __repr__(self):
        """Display it."""
        repr_str = f"{self.short_sha} ('{self.title}')"
        author = self.author or f'{self.author_name} ({self.author_email})'
        committer = f'{self.committer_name} ({self.committer_email})'
        repr_str += f', author: {author}, committer: {committer}, dco: {self.dco_state.name}'
        return f'<Commit {repr_str}>'

    @property
    def dco_state(self):
        """Return the DCOState of the Commit."""
        # There were no signoffs found in the commit description.
        if not self.description.signoff or not self.expected_signoff:
            return DCOState.MISSING
        # unidecode the expected signoff.
        # https://github.com/pylint-dev/pylint/issues/1498
        # pylint: disable=unsubscriptable-object
        expected_signoff = (unidecode(self.expected_signoff[0]), self.expected_signoff[1])
        # At least one of the expected signoffs matches a Description.signoff.
        description_signoffs = [(unidecode(name), mail) for name, mail in self.description.signoff]
        if expected_signoff in description_signoffs:
            # OK if there is an @redhat.com signoff, otherwise NOT_REDHAT.
            return DCOState.OK if expected_signoff[1].endswith('@redhat.com') \
                else DCOState.OK_NOT_REDHAT
        state = DCOState.UNRECOGNIZED
        # Do any of the commit DCOs have a name that matches the commit commiter name?
        if any(dco for dco in description_signoffs if dco[0] == expected_signoff[0]):
            state = DCOState.NAME_MATCHES
        # ... or do any of the commit DCOs have an email address that matches the committer email?
        elif any(dco for dco in description_signoffs if dco[1] == expected_signoff[1]):
            state = DCOState.EMAIL_MATCHES
        return state

    @property
    def expected_author_signoff(self) -> typing.Union[Signoff, None]:
        """Return the expected signoff for the author."""
        return (self.author_name, self.author_email) if \
            self.author_name and self.author_email else None

    @property
    def expected_committer_signoff(self) -> typing.Union[Signoff, None]:
        """Return the expected signoff for the committer."""
        return (self.committer_name, self.committer_email) if \
            self.committer_name and self.committer_email else None

    @property
    def expected_signoff(self) -> typing.Union[Signoff, None]:
        """Return a tuple with the expected DCO signoff depending on self.zstream, or None."""
        return self.expected_committer_signoff if self.zstream else self.expected_author_signoff

    @property
    def short_sha(self):
        """Return the first 12 chars of the sha."""
        return self.sha[:12]
